<?php

namespace App;

use App\Models\Follow;
use App\Models\Timeline;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens , Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

   /**
    * Get all of the messages for the User
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function messages(): HasMany
   {
       return $this->hasMany(Messages::class);
   }

   /**
    * Get the setting associated with the User
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
   public function setting(): HasOne
   {
       return $this->hasOne(Settings::class);
   }
   /**
    * Get all of the match for the User
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function pendingMatch(): HasMany
   {
       return $this->hasMany(Match::class, 'to_user', 'id')->where('status','pending');
   }
   /**
    * Get all of the notification for the User
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function notification(): HasMany
   {
       return $this->hasMany(Notification::class);
   }

   /**
    * Get all of the deviceLogin for the User
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function deviceLogin(): HasMany
   {
       return $this->hasMany(DeviceLogin::class);
   }
}
