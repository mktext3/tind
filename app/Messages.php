<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    /**
     * Get the user that owns the Messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function touser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'to_user');
    }
    /**
     * Get the user that owns the Messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fromuser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'from_user');
    }
}
