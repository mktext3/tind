<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceLogin extends Model
{
    /**
     * Get the user that owns the DeviceLogin
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
