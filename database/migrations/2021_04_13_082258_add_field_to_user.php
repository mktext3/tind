<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('gender', ['male', 'female','other'])->nullable();
            $table->string('sexuality', 100)->nullable();
            $table->string('preference')->nullable();
            $table->string('Phone')->nullable();
            $table->decimal('lat', 5, 2)->nullable();
            $table->decimal('long', 5, 2)->nullable();
            $table->string('age_preference')->nullable();
            $table->string('city')->nullable();
            $table->string('date_style')->nullable();
            $table->string('lnstagram_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('country')->nullable();
            $table->string('pincode')->nullable();
            $table->text('bio')->nullable();
            $table->integer('age')->unsigned()->nullable()->default(14);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('sexuality');
            $table->dropColumn('preference');
            $table->dropColumn('Phone');
            $table->dropColumn('lat');
            $table->dropColumn('long');
            $table->dropColumn('age_preference');
            $table->dropColumn('date_style');
            $table->dropColumn('lnstagram_id');
            $table->dropColumn('facebook_id');
            $table->dropColumn('city');
            $table->dropColumn('country');
            $table->dropColumn('pincode');
            $table->dropColumn('bio');
            $table->dropColumn('age');
        });
    }
}
