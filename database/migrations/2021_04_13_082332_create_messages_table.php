<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('to_user')->default(0);
            $table->bigInteger('from_user')->default(0);
            $table->integer('type')->unsigned()->default(0);
            $table->string('content')->nullable();
            $table->text('large')->nullable();
            $table->bigInteger('replied_id')->default(0);
            $table->boolean('is_forwarded')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
